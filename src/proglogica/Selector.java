package proglogica;

/**
 *
 * @author dsk8
 */
public class Selector {
    private final int HORAS_TURNO = 6;
    private Dia semana[];
    private Empleado empleados[];
    private Empleado empleado[];
    private Actividad actividades[];
    private Dia random[];
    private NumeroAle nums = new NumeroAle();
    
    public Selector() {
        
        this.semana = new Dia[] {
            new Dia("Lunes"),
            new Dia("Martes"),
            new Dia("Miercoles"),
            new Dia("Jueves"),
            new Dia("Viernes"),
            new Dia("Sabado")
        };
        
        this.empleados = new Empleado[] {
            new Empleado("Humberto Hernandez", true, true),
            new Empleado("Juan Garcia", true, true),
            new Empleado("Roberto Aparicio", true, true),
            new Empleado("Joaquin Beltran", true, true),
            new Empleado("Jair Quevedo", true, true),
            new Empleado("Regina Garcia", true, false),
            new Empleado("Brenda Galicia", true, false),
            new Empleado("Sara Connor", true, false),
            new Empleado("Liliana Gonzales", true, false),
            new Empleado("Maria Juarez", true, false),
            new Empleado("Ricardo Dominguez", false, true),
            new Empleado("David Moreno", false, true),
            new Empleado("Alberto Palma", false, true),
            new Empleado("Ezequiel Merida", false, true),
            new Empleado("Alan Saldana", false, true),
            new Empleado("Sarai Torres", false, false),
            new Empleado("Alejandra Cruz", false, false),
            new Empleado("Selena Martinez", false, false),
            new Empleado("Viviana Flores", false, false),
            new Empleado("Esther Rojas", false, false)
        };
        
        this.actividades = new Actividad[] {
            new Actividad("Limpieza de oficinas.", 1),
            new Actividad("Registro y archivo de correspondencia.", 1),
            new Actividad("Limpieza de espacios exteriores.", 1),
            new Actividad("Podar jardines.", 2),
            new Actividad("Podar arboles.", 2),
            new Actividad("Regar areas verdes.", 2),
            new Actividad("Limpieza de sanitarios.", 3),
            new Actividad("Compras.", 3),
            new Actividad("Control y registro.", 6),
            new Actividad("Mantenimiento de vehiculos.", 4),
            new Actividad("Mantenimiento electrico.", 4),
            new Actividad("Mantenimiento hidraulico.", 5),
            new Actividad("Mantenimiento de inmuebles.", 5),
            new Actividad("Mantenimiento de mobiliario.", 5)
        };
        
    }
    
    public void generarHorario() {
        int i, j, k;
        boolean paso = true;
        
        for(i = 0;i < 1;i++) {
            for(j = 0;j < 10;j++) {
                if(j < 3) {
                    for(k = 0;k < this.actividades.length ;k++) {
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                            else{
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                        }
                                        else {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                            else{
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    for(k = this.actividades.length - 1;k >= 0 ;k--) {
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                this.actividades[k].setDisponible();
                                            }
                                            else {
                                                this.empleados[j].agregarActividad(this.actividades[k]); 
                                                this.actividades[k].setDisponible();
                                            }
                                        }
                                        else {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                this.actividades[k].setDisponible();
                                            }
                                            else{
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                                this.actividades[k].setDisponible();
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                
                else {
                    for(k = this.actividades.length - 1;k >= 0 ;k--) {
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(this.actividades[k].getDisponible()) {
                                            if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                                if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                                else {
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                            }
                                            else {
                                                if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                                else{
                                                    this.empleados[j].agregarActividad(this.actividades[k]);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        if(this.actividades[k].getNombre().compareTo("Regar areas verdes.") == 0)
                            this.actividades[k].setDisponible();
                    }
                    
                    for(k = this.actividades.length - 1;k >= 0 ;k--) {
                        if(j == 3 || j == 4) {
                            this.actividades[5].setDisponible();
                        }
                        if(this.actividades[k].getDisponible()) {
                            if(this.actividades[k].getHoras_trabajo() <= getHorasLibres(this.empleados[j])) {
                                if(!actividadRepetida(this.actividades[k], j)) {
                                    if(!actividadChoca((this.empleados[j].getHoras()), i, this.actividades[k])) {
                                        if(disponibleRegar(this.empleados[j].getHoras(), this.actividades[k])) {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo())
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                            else
                                                this.empleados[j].agregarActividad(this.actividades[k]);                                               
                                        }
                                        else {
                                            if(getSoloHombres(this.actividades[k]) && this.empleados[j].getSexo()) {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                            }
                                            else {
                                                this.empleados[j].agregarActividad(this.actividades[k]);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                this.semana[i].setEmpleado(this.empleados[j]);
            }
        }
        for(i = 0;i < 6;i++) {
            for(j = 10;j < 20;j++) {
                this.semana[i].setEmpleado(this.empleados[j]);
            }
        }
        
        int aux = 10;
        
        for(i = 0;i < 1;i++) {
            for(j = 0;j < 10;j++, aux++) {
                for(k = this.semana[i].empleados.get(j).acts.size() - 1;k >= 0;k--) {
                    this.semana[i].empleados.get(aux).agregarActividad(this.semana[i].empleados.get(j).acts.get(k));
                }
            }
            aux = 10;
        }
        
        Dia n = this.semana[0];
        
        for(i = 0;i < 6;i++) {
            this.semana[i] = n;
        }
        
        this.random = this.semana;
        
        int a[];
        int b;
        
        for(i = 0;i < 6;i++) {
            a = nums.numero(0, 5);
            for(j = 0, b = 0;j < 5;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.semana[i].empleados.get(a[b]).getNombre());
            }
            a = nums.numero(5, 10);
            for(j = 5, b = 0;j < 10;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.semana[i].empleados.get(a[b]).getNombre());
            }
            
            a = nums.numero(10, 15);
            for(j = 10, b = 0;j < 15;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.semana[i].empleados.get(a[b]).getNombre());
            }
            
            a = nums.numero(15, 20);
            for(j = 15, b = 0;j < 20;j++, b++) {
                this.random[i].empleados.get(j).setNombre(this.semana[i].empleados.get(a[b]).getNombre());
            }
        }
        
        for(k = 0;k < 6;k++) {
            for(j = 0;j < 20;j++) {
                System.out.println(this.random[k].empleados.get(j).getNombre());
                
                System.out.println(""); 
            }
        }
    }
    
    public boolean disponibleRegar(int hora, Actividad actividad) {
        return actividad.getNombre().compareTo("Regar areas verdes.") == 0 && hora == 0;
    }
    
    public boolean actividadRepetida(Actividad actividad, int index) {
        for(int i = 0;i < 6;i++) {
            if(this.empleados[index].actividades[i].compareTo(actividad.getNombre()) == 0)
                return true;
        }
        
        return false;
    }
    
    public boolean actividadChoca(int hora, int dia, Actividad actividad) {
        int cont = 0;
        for(int i = 0;i < this.semana[dia].empleados.size();i++) {
            if(this.semana[dia].empleados.get(i).actividades[hora].compareTo(actividad.getNombre()) == 0)
                cont++;
        }
        
        return cont > 0;
    }
    
    public int getHorasLibres(Empleado empleado) {
        return HORAS_TURNO - empleado.getHoras();
    }
    
    public boolean getSoloHombres(Actividad actividad) {
        if(actividad.getNombre().contains("Mantenimiento"))
            return true;
        else
            return false;
    }
}
