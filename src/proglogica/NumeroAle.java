package proglogica;

/**
 *
 * @author Brandon Gonzalez
 */
public class NumeroAle {
    
    NumeroAle(){
        
    }

    public int[] numero(int nInicial, int nFinal) {

        int[] aleatorios = new int[nFinal - nInicial];

        int num = nInicial;
        Numero lis[] = new Numero[aleatorios.length];
        for (int i = 0; i < lis.length; i++, num++) {
            lis[i] = new Numero(num, true);
        }

        int n = (int) Math.floor(Math.random() * (0 - aleatorios.length + 1) + aleatorios.length);

        int cont = 0;

        for (;;) {
            if (lis[n].disponible) {
                aleatorios[cont] = lis[n].numero;
                //System.out.println("Numero: " + lis[n].numero + " n: " + n);
                lis[n].disponible = false;
                cont++;
            } else if (cont == aleatorios.length) {
                break;
            }
            n = (int) Math.floor(Math.random() * (-1 - aleatorios.length + 1) + aleatorios.length);
        }

        return aleatorios;
    }

    public class Numero {

        int numero;
        boolean disponible;

        Numero(int n, boolean estdo) {
            this.numero = n;
            this.disponible = estdo;
        }
    }

}
